import * as ActionTypes from "./ActionTypes";

export function closeAlert(status) {
    return { type: ActionTypes.CLOSE_STATUS, status };
}

const buildErrorPayload = (message) => ({ message, type: "error", isOpen: true });
const buildInfoPayload = (message) => ({ message, type: "info", isOpen: true });
const buildSuccessPayload = (message) => ({ message, type: "success", isOpen: true });

export const infoAlert = (message) => ({ type: ActionTypes.INFO_STATUS, payload: buildInfoPayload(message) })
export const successAlert = (message) => ({ type: ActionTypes.SUCCESS_STATUS, payload: buildSuccessPayload(message) });
export const errorAlert = (message) => ({ type: ActionTypes.ERROR_STATUS, payload: buildErrorPayload(message) })
