import * as ActionTypes from './ActionTypes';
import * as ApiActions from './ApiActions';
import * as AlertActions from './AlertActions';
import { EVENT_URL } from './url';

export const fetchEventList = () => {
    async dispatch => {
        dispatch(fetchEventListBegine());
        return ApiActions.httpGET(EVENT_URL)
            .then(json => dispatch(fetchEventListSuccess(json)))
            .catch(response => {
                dispatch(fetchEventListFailure(response));
                dispatch(AlertActions.errorAlert("ERROR"));
            });
    }
}

const fetchEventListBegine = () => ({ type: ActionTypes.FETCH_EVENT_LIST_BEGINE });
const fetchEventListSuccess = (payload) => ({ type: ActionTypes.FETCH_EVENT_LIST_SUCCESS, payload });
const fetchEventListFailure = (payload) => ({ type: ActionTypes.FETCH_EVENT_LIST_FAILURE, payload });
