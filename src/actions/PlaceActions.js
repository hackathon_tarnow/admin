import * as ActionTypes from './ActionTypes';
import * as ApiActions from './ApiActions';
import { PLACE_URL } from './url';

export const fetchPlaceList = () => (
    dispatch => {
        dispatch(fetchPlaceListBegine());
        return ApiActions.httpGET(PLACE_URL)
            .then(json => dispatch(fetchPlaceListSuccess(json)))
            .catch(response => {
                dispatch(fetchPlaceListFailure(response));
            });
    }
)

const fetchPlaceListBegine = () => ({ type: ActionTypes.FETCH_PLACE_LIST_BEGINE });
const fetchPlaceListSuccess = (payload) => ({ type: ActionTypes.FETCH_PLACE_LIST_SUCCESS, payload });
const fetchPlaceListFailure = (payload) => ({ type: ActionTypes.FETCH_PLACE_LIST_FAILURE, payload });
