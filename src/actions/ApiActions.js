export const httpGET = (url) => (
    new Promise((resolve, reject) => (
        fetch(url)
            .then(handleErrors)
            .then(mapResponse)
            .then(resolve)
            .catch(reject)
    ))
);

export const httpDELETE = (url) => (
    new Promise((resolve, reject) => (
        fetch(url, { method: 'DELETE' })
            .then(handleErrors)
            .then(resolve)
            .catch(reject)
    ))
);

export const httpPOST = (url, element = {}) => (
    new Promise((resolve, reject) => (
        fetch(url, {
            method: 'POST',
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify(element)
        }).then(handleErrors)
            .then(mapResponse)
            .then(resolve)
            .catch(reject)
    ))
);

export const httpPUT = (url, element) => (
    new Promise((resolve, reject) => (
        fetch(url, {
            method: 'PUT',
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify(element)
        }).then(handleErrors)
            .then(mapResponse)
            .then(resolve)
            .catch(reject)
    ))
);

export const handleErrors = (response) => {
    if (response && !response.ok) {
        throw Error(response.statusText);
    }
    return response;
}

export const mapResponse = response => {
    const contentType = response.headers.get("content-type");
    if (contentType && contentType.indexOf("application/json") !== -1) {
        return response.json();
    } else {
        return response.text();
    }
};