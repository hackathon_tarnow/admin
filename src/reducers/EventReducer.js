import * as ActionTypes from "../actions/ActionTypes";

const INITIAL_STATE = {
    listItems: { items: [], loading: false },
    item: { item: {}, loading: false },
}

export default (state = INITIAL_STATE, action) => {
    console.log(state, action)
    switch (action.type) {
        case ActionTypes.FETCH_EVENT_LIST_BEGINE:
            return { ...state, listItems: { items: [], loading: true } };
        case ActionTypes.FETCH_EVENT_LIST_SUCCESS:
            return { ...state, listItems: { items: action.payload, loading: false } };
        case ActionTypes.FETCH_EVENT_LIST_FAILURE:
            return { ...state, listItems: { items: [] }, loading: false };
        default:
            return state;
    }
}