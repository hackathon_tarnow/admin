import { combineReducers } from 'redux';
import EventReducer from './EventReducer';
import PlaceReducer from './PlaceReducer';

const rootReducer = combineReducers({
    EventReducer, PlaceReducer
})

export default rootReducer;