import React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import QRCode from 'qrcode.react';
import { IconButton } from '@material-ui/core';
import LineStyle from '@material-ui/icons/LineStyle';

class QRDialog extends React.Component {
  state = {
    open: false,
  };
  QRCode = require('qrcode.react');

  handleClickOpen = () => {
    this.setState({ open: true });
  };

  handleClose = () => {
    this.setState({ open: false });
  };

  render() {
      const {id, name} = this.props;
    return (
      <div>
        <IconButton aria-label="Delete" onClick={this.handleClickOpen}>
            <LineStyle />
        </IconButton> 
        <Dialog
          open={this.state.open}
          onClose={this.handleClose}
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description"
        >
          <DialogTitle id="alert-dialog-title">{name}</DialogTitle>
          <DialogContent>
          <QRCode value={`PLACE:${id}`} size='256'/>
          </DialogContent>
          <DialogActions>
            <Button onClick={this.handleClose} color="primary">
              Zamknij
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    );
  }
}

export default QRDialog;