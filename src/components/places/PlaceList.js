import React, { Component, Fragment } from "react";
import { bindActionCreators } from "redux";
import { connect } from 'react-redux'
import * as PlaceActions from '../../actions/PlaceActions'
import { Grid, Avatar, Card, CardContent, List, CardHeader, ListItem, ListItemText, ListItemSecondaryAction, ListItemAvatar, IconButton, TextField } from '@material-ui/core';
import Event from '@material-ui/icons/Event';
import * as _ from 'lodash';
import QRDialog from "./QRDialog";

class PlaceList extends Component {

    constructor(props) {
        super(props);
        this.state = {
            searchValue: ""
        };
        this.onSearch = _.debounce(this.onSearch, 150);
    }

    handleSearch = name => ({ target: { value } }) => {
        this.setState({
            ...this.state,
            [name]: value
        });
        this.onSearch(value);
    }

    componentDidMount() {
        const { placeActions: { fetchPlaceList } } = this.props;
        fetchPlaceList();
    }

    onSearch = searchValue => {
        this.setState({ searchValue });
    }

    buildListItem = (item, itemId) => {
        return (
            <Fragment key={`li-${itemId}`} >
                <ListItem>
                    <ListItemAvatar>
                        <Avatar>
                            <Event />
                        </Avatar>
                    </ListItemAvatar>
                    <ListItemText
                        primary={item.name}
                        secondary={item.content}
                    />
                    <ListItemSecondaryAction>
                        <QRDialog id={item.id} name={item.name} />
                    </ListItemSecondaryAction>
                </ListItem>
            </Fragment>
        )
    }

    render() {
        const { list: { items, loading } } = this.props;
        const { searchValue } = this.state;
        return (
            <Grid container spacing={16}>
                <Grid item xs={12}>
                    <Card>
                        <CardHeader
                            title={"Lista miejsc"}
                            subheader={"Lista zarejestrowanych lokacji w Tarnowie"} />
                        <CardContent>
                            <TextField
                                id="standard-full-width"
                                label="Szukaj"
                                style={{ margin: 8 }}
                                placeholder=""
                                fullWidth
                                onChange={this.handleSearch("searchValue")}
                                margin="normal"
                            />
                            {/* <SearchTextField onSearch={this.onSearch} /> */}
                            <List dense id="tracked_listaaaaa">
                                {items.filter(item =>
                                    item.name.toLowerCase().includes(searchValue.toLowerCase())
                                ).map((item, itemId) => (
                                    this.buildListItem(
                                        item,
                                        itemId,
                                        items.length === itemId + 1
                                    )))}
                            </List>
                        </CardContent>
                    </Card>
                </Grid>
            </Grid>
        )
    }
}

const mapStateToProps = (state, props) => ({ ...props, list: state.PlaceReducer.listItems });

const mapDispatchToProps = dispatch => ({
    placeActions: bindActionCreators(PlaceActions, dispatch),
})
export default connect(mapStateToProps, mapDispatchToProps)(PlaceList);