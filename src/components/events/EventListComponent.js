import React, { Component, Fragment } from "react";
import { bindActionCreators } from "redux";
import { connect } from 'react-redux'
import * as EventActions from '../../actions/EventActions'
import { CardHeader, CardContent } from "@material-ui/core";
import { Grid, Card, CardContent, List, ListItem, ListItemText, CardHeader, Avatar, withStyles, ListItemIcon, Tooltip, IconButton } from '@material-ui/core';


class EventListComponent extends Component {

    render() {
        const { classes, list: { items, loading } } = this.props;

        return (
            <Card>
                <CardHeader
                    title={"Lista wydarzeń"}
                    subheader={"Dostępne wydarzenia na w aplikacji"}/>
                <CardContent>
                    {loading && <LinearProgress />}
                    <div className={classes.root}>
                        <SearchTextField onSearch={this.onSearch} />
                        <List dense id="tracked_listaaaaa">
                            {items
                                .filter(item => item.name.toLowerCase().includes(this.state.searchText.toLowerCase()))
                                .slice(0, this.state.pageSize)
                                .map((item, itemId) => (
                                    this.buildListItem(
                                        item,
                                        itemId,
                                        items.length === itemId + 1
                                    )))}
                        </List>
                    </div>
                </CardContent>
            </Card>

        )
    }
}
const mapStateToProps = (state, props) => ({ ...props, list: state.events.listItems });


const mapDispatchToProps = dispatch => ({
    confirmActions: bindActionCreators(EventActions, dispatch),
})
export default connect(mapStateToProps, mapDispatchToProps)(EventListComponent);