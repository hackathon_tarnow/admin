import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import { Router } from 'react-router-dom'
import * as serviceWorker from './serviceWorker';
import configureStore from './store/ConfigureStore';
import { Provider } from "react-redux";
import history from './history';

const store = configureStore();

ReactDOM.render((
    <Provider store={store}>
        <Router history={history}>
            <App /> 
        </Router>
    </Provider>), document.getElementById('root'));

serviceWorker.unregister();