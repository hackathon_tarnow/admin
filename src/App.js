import React, { Component } from 'react';
import './App.css';
import { withRouter } from "react-router";
import PlacesList from './components/places/PlaceList';
import { Grid } from '@material-ui/core';

class App extends Component {
  render() {
    return (
      <div className="App">
        <Grid container spacing={24}>
          <Grid item xs={6}>
            <PlacesList></PlacesList>
          </Grid>
        </Grid>
      </div>
    );
  }
}
export default withRouter(App);
